## 无身份验证的 registry + docker-registry-web 配置示例

### 如何运行：

1. 检出此目录的所有代码
    
2. 使用 docker-compose 启动容器：
    
        docker-compose up

运行成功后，创建的私有仓库地址是 `127.0.0.1:5000` ， WebUI 管理地址为 `http://127.0.0.1:8080/`
