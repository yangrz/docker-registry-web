# Docker 私有仓库搭配界面化管理

原代码来自： https://github.com/mkuchin/docker-registry-web/tree/master/examples

原文有些老旧和纰漏，本文做了一些修正。


## 界面化管理的三种方案
  
  *  方案1：[无身份认证 - 匿名访问](auth-disabled/)
  *  方案2：[启用身份认证](auth-enabled/)
  *  方案3：[启用身份认证 - Nginx代理](nginx-auth-enabled/)
