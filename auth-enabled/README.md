## 启用身份认证的 registry + docker-registry-web 配置示例

### 如何运行:

1. 检出此目录的所有代码
        
2. 使用脚本生成私钥和自签名证书：
    
        ./generate-keys.sh
    
3. 使用 docker-compose 启动容器
    
        docker-compose up

运行成功后，创建的私有仓库地址是 `127.0.0.1:5000` ， WebUI 管理地址为 `http://127.0.0.1:8080/`

### 校验是否有效：
  
1. 登录 `http://127.0.0.1:8080/` ，管理员账号是 `admin`，密码是 `admin`
2. 创建测试用户并授予该用户“write-all”角色。
3. 在终端分别执行：
        
        docker login 127.0.0.1:5000
        docker pull hello-world
        docker tag hello-world 127.0.0.1:5000/hello-world:latest
        docker push 127.0.0.1:5000/hello-world:latest
        docker rmi 127.0.0.1:5000/hello-world:latest
        docker run 127.0.0.1:5000/hello-world:latest
